//
//  Messages.swift
//  Flash Chat iOS13
//
//  Created by Alexader Malygin on 01.05.2020.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Messages {
    let sender: String
    let body: String
}
